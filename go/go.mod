module gitlab.com/gitlab-org/gitlab-shell/go

go 1.12

require (
	github.com/mattn/go-shellwords v0.0.0-20190425161501-2444a32a19f4
	github.com/otiai10/copy v1.0.1
	github.com/otiai10/curr v0.0.0-20190513014714-f5a3d24e5776 // indirect
	github.com/sirupsen/logrus v1.0.5
	github.com/stretchr/testify v1.2.2
	gitlab.com/gitlab-org/gitaly v1.7.0
	gitlab.com/gitlab-org/gitaly-proto v1.12.0
	gitlab.com/gitlab-org/labkit v0.0.0-20190221122536-0c3fc7cdd57c
	golang.org/x/crypto v0.0.0-20180312195533-182114d58262 // indirect
	golang.org/x/sys v0.0.0-20181206074257-70b957f3b65e // indirect
	google.golang.org/genproto v0.0.0-20181202183823-bd91e49a0898 // indirect
	google.golang.org/grpc v1.16.0
	gopkg.in/DataDog/dd-trace-go.v1 v1.9.0 // indirect
	gopkg.in/airbrake/gobrake.v2 v2.0.9 // indirect
	gopkg.in/gemnasium/logrus-airbrake-hook.v2 v2.1.2 // indirect
	gopkg.in/yaml.v2 v2.2.1
)
